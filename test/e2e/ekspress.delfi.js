const config = require('../../nightwatch.conf.js');

module.exports = { // Addapted from: https://git.io/vodU0
  '@tags': ['ekspress'],

  'Open first article on ekspress.': function (browser) {
    browser
      .windowMaximize()
      // Load ekspress.delfi.ee
      .url('https://ekspress.delfi.ee/')
      .waitForElementVisible('body')
      // Take screenshot of main page.
      .saveScreenshot(`${config.imgpath(browser)}ekspress-mainPage.png`)
      .click(".headline__picture:first-child")
      .waitForElementVisible('body')
      // Take screenshot of first article.
      .saveScreenshot(`${config.imgpath(browser)}ekspress-firstArticle.png`)
      .end();
  },

  'Expect ekspress latest news is visible': function (browser) {
    browser
      .windowMaximize()
      .url('https://ekspress.delfi.ee/')
      .waitForElementVisible('body')
      .expect.element('.latest-news:first-child').to.be.visible;

    browser.end();
  },

  'Using eksress search': function (browser) {
    var search = "nightwatch";

    browser
      .windowMaximize()
      .url('https://ekspress.delfi.ee/')
      .waitForElementVisible('body')
      .pause(1000)
      .click('.header-search__input:first-child')
      .setValue('.header-search__input:first-child', [search, browser.Keys.ENTER])
      .pause(1000)
      .waitForElementVisible('body')
      .assert.containsText('body', search)
    browser.end();
  },
};